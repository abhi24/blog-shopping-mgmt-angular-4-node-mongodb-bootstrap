# README #

This WebSite is developed using **MEAN Stack**
This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###
This repository is a **Blog/Shopping Cart WebSite** where users can
> ### User
> 
> 1.   Post Blogs/ Edit Blogs
> 2.   See other persons blog and comment/like/dislike it.
> 3.   Purchase games
>


## Getting Started ##

### Running the Project

We're using Node.js, NPM (Node Package Manager) to manage the code in this repo. To preview code locally, you'll need to install Node and NPM, then run the following commands from a terminal window, in the repo directory:

> $ npm install inside the client folder as it will install all the dependencies
>
> $ ng serve --port 4040
>
> $ nodemon at the parent folder as it will connect node.js

Those commands do the following:

npm install will install the necessary node.js packages to develop on this project
Once run, you can preview the site by pointing a web server at the dist directory (see below).

ng serve will run the code on local host server 4200

### Downloading the project

Download the entire project and go to the run ng serve from the client folder.
Also run nodemon from the parent folder in a separate terminal
To test the project enter the username: **test** and password: **test** and then from login fetch all the data from the database first.

## Images
>
   **Main Screen**
>
![Alt text](https://bytebucket.org/abhi24/blog-shopping-cart-web-site/raw/7d81cac5ccc02614c739bc329a90bec34686427a/Blog%20Shopping%20Cart%20Web%20Site/Screen%20Shot%202017-09-09%20at%2010.03.54%20PM.png?token=b609a2b9b5a37ab6e465f7c2d302665e71afc874)

>
  **Products Page**
>
![Alt text](https://bytebucket.org/abhi24/blog-shopping-cart-web-site/raw/7d81cac5ccc02614c739bc329a90bec34686427a/Blog%20Shopping%20Cart%20Web%20Site/Screen%20Shot%202017-09-09%20at%2010.04.27%20PM.png?token=1acfd9378e736b6f5662c1b98f0133589fd36748)

>
   **Shopping Cart** 
>
![Alt text](https://bytebucket.org/abhi24/blog-shopping-cart-web-site/raw/7d81cac5ccc02614c739bc329a90bec34686427a/Blog%20Shopping%20Cart%20Web%20Site/Screen%20Shot%202017-09-09%20at%2010.04.54%20PM.png?token=622b0b0cb3469632b177a9678d1a33efe68caecd) 

>
   **Blogs Page**
>
![Alt text](https://bytebucket.org/abhi24/blog-shopping-cart-web-site/raw/7d81cac5ccc02614c739bc329a90bec34686427a/Blog%20Shopping%20Cart%20Web%20Site/Screen%20Shot%202017-09-09%20at%2010.11.05%20PM.png?token=5d95facb32a26b8008819871e45dca37c821625f) 


## Who do I talk to? ###

> Abhishek Gupta
>