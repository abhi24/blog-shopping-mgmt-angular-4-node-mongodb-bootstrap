var express = require('express');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var cors = require('cors');

var blogs = require('./routes/blogs');
var authentication = require('./routes/authentication');
var shops = require('./routes/shopping');

var bodyParser = require('body-parser');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://test:test@ds161713.mlab.com:61713/nodetodo', function(){
    console.log('Connected to Db');
});

app.use(cors({
    origin: 'http://localhost:4200'
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(__dirname + '/client/dist/'));

app.use('/authentication', authentication);
app.use('/blogs', blogs);
app.use('/shops', shops);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/client/dist/index.html'));
});


app.listen(3000, () => {
    console.log('Server Running');
});