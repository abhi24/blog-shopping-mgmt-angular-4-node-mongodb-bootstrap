var Shop = require('../models/shop');
var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://test:test@ds161713.mlab.com:61713/nodetodo', ['shops']);
var users = []; 

router.post('/saveproduct', (req, res) => {
    if(!req.body.name) {
        res.json({success: false , message: 'No name present'});        
    } else {
        if(!req.body.amount) {
            res.json({success: false , message: 'No amount present'});
        } else {
             if(!req.body.description) {
                res.json({success: false , message: 'No Description present'});   
            } else {
                if(!req.body.imagePath) {
                    res.json({success: false , message: 'No Image Path present'});                       
                } else {
                    const prod = new Shop({
                        imagePath: req.body.imagePath,
                        name: req.body.name,
                        description: req.body.description,
                        amount: req.body.amount,
                    });

                    prod.save(prod, function(err , data) {
                        if(err){
                            res.json({success: false, message: err});                    
                        } else {
                            res.json({success: true, message: 'Blog Saved'});  
                        }
                    });
                }
            }
        }
    }
});

router.get('/getProduct/:id', (req, res) => {
    db.shops.findOne({_id: mongojs.ObjectId(req.params.id)}, (err, data) => {
        if(err){
            res.json({success: false, message: err}); 
        }else {
            res.json({success: true, message: data}); 
        }
    })
});


router.get('/getAllProducts', (req, res) => {
    db.shops.find({}, (err, data) => {
        if(err) {
            res.json({success: false, message: err});     
        } else {
            res.send(data);
        }
    });
});

module.exports = router;