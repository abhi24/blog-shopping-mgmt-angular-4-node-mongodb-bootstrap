var User = require('../models/user');
var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var jwt = require('jsonwebtoken');

var db = mongojs('mongodb://test:test@ds161713.mlab.com:61713/nodetodo', ['users']);

router.post('/register', (req, res) => {
    if(!req.body.email) {
        res.json({success: false, message: 'Provide an email'});
    }  else {
        if(!req.body.username) {
            res.json({success: false, message: 'Provide an username'});
        } else {
            if(!req.body.password) {
                res.json({success: false, message: 'Provide an password'});
            } else {
                    var newUser = new User({
                        email: req.body.email.toLowerCase(),
                        username: req.body.username.toLowerCase(),
                        password: req.body.password
                    });
                    newUser.save((err) => {
                        if(err) {
                            if(err.code === 11000) {
                                res.json({success: false, message: 'Duplicate Username'});                                
                            } else {
                                res.json({success: false, message: 'Could not save user', err});                                
                            }
                        } else {
                            res.json({success: true, message: 'User Saved'});
                        }
                    });
            }
        }
    }
});
    
router.post('/login', (req, res) => {
    if(!req.body.username){
        res.json({success: false, message: 'No username provided'});
    } else {
        if(!req.body.password) {
            res.json({success: false, message: 'No password provided'});
        }
        else {
            db.collection('users').findOne({ username: req.body.username}, (err, user) => {
                if(!user) {
                    res.json({success: false, message: 'No user found'});
                } else {
                    if(req.body.password === user.password ) {
                     const token =   jwt.sign({userId : user._id}, user.username, {expiresIn: '1h'} );
                     const usersss= user.username; 
                     const passss= user.password;  
                     res.json(
                            {success: true,
                             message: 'Success',
                             token: token,
                             val: user,
                             usersss,
                             passss,
                             user: {username: user.username}
                            });
                    } else {
                        res.json({success: false, message: 'Invalid password'});
                    }
                }
            });
        }
    }
});


router.post('/profile', (req, res) => {
    if(!req.body.username){
        res.json({success: false, message: 'No username provided'});
    } else {
        db.collection('users').findOne({ username: req.body.username}, (err, user) => {
            if(!user) {
                res.json({success: false, message: 'No user found'});
        } else {
                res.json(
                    {success: true,
                     message: 'Success',
                     user: {username: user.username},
                     pass: {password: user.password},
                     email: {email: user.email}
                    });
            
        }
    });
}
});



module.exports = router;
