var Blog = require('../models/blog');
var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://test:test@ds161713.mlab.com:61713/nodetodo', ['blogs']);
var dbuser = mongojs('mongodb://test:test@ds161713.mlab.com:61713/nodetodo', ['users']);

router.post('/newBlog', (req, res) => {
    if(!req.body.title){
        res.json({success: false, message: 'No title'});
    } else {
        if(!req.body.body) {
            res.json({success: false, message: 'No Body'});
        } else {

        } if(!req.body.createdBy) {
            res.json({success: false, message: 'No createdby'});
        } else {
            var newB = new Blog({
                title: req.body.title,
                body: req.body.body,
                createdBy: req.body.createdBy,
            });
            newB.save((err) => {
                if(err){
                    res.json({success: false, message: err});                    
                } else {
                    res.json({success: true, message: 'Blog Saved'});  
                }
            });
        }
    }
})

router.get('/allBlogs', (req, res) => {
    db.blogs.find({}, (err, data) => {
        if(err){
            res.json({success: false, message: err}); 
        }else {
            res.json({success: true, message: data}); 
        }
    })
});

router.get('/singleBlog/:id', (req, res) => {
    db.blogs.findOne({_id: mongojs.ObjectId(req.params.id)}, (err, data) => {
        if(err){
            res.json({success: false, message: err}); 
        }else {
            res.json({success: true, message: data}); 
        }
    })
});


router.put('/updateBlog', (req, res) => {
    if(!req.body._id) {
     res.json({success: false, message: 'No Blog id provided'}); 
    } else {
     db.blogs.findOne({_id: mongojs.ObjectId(req.body._id)}, (err, data) => {
         if(err){
             res.json({success: false, message: 'No id found'}); 
         }else {
    
            var newvalues =  { $set: { 'title': req.body.title ,  'body': req.body.body} };
            db.collection('blogs').update({_id: mongojs.ObjectId(req.body._id)}, { $set: { 'title': req.body.title , 'body': req.body.body} }, function(err, data)  {
                if (err) {
                    throw err;   
                }
                else {
                    console.log("1 document updated");
                    res.json({success: true, message: 'Updated'});                   
                }
             } );
         }
     });
    }
 });

router.put('/deleteBlog/:id', (req, res) => {
    if(!req.body.id) {
        res.send('No id present');
    } else {
        db.collection('blogs').remove({_id: mongojs.ObjectId(req.body.id)}, (err, data) => {
            if(err) {
                res.json({success: false, message: 'No id found'}); 
            } else {
                res.json({success: true, message: 'Deleted'}); 
            }
        });
    }
});


router.put('/likeBlog', (req, res) => {
    if(!req.body.id) {
        res.json({success: false, message: 'No id found'});
    }
    else if(!req.body.name) {
        res.json({success: false, message: 'No name found'});
    } else {
        db.collection('blogs').findOne({_id: mongojs.ObjectId(req.body.id)} , (err, blog) => {
            if(err) {
                res.json({success: false, message: 'Invalid blog id'}); 
            } else {
                if(!blog) {
                    res.json({success: false, message: 'Blog not found'}); 
                } else {
                    if(blog.likedBy.includes(req.body.name))  {                     
                        res.json({success: false, message: 'You have aleady liked it'});   
                     } else {
                        if(blog.dislikedBy.includes(req.body.name)) {
                        
                            var a = blog.likes;
                            a++;
                            var b = blog.dislikes;
                            b--;
                            db.collection('blogs').update({_id: mongojs.ObjectId(req.body.id)},
                             { $push: { 'likedBy': req.body.name},
                              $set: {'likes': a, 'dislikes': b}, 
                              $pull: { 'dislikedBy': req.body.name} },

                              function(err, data)  {
                                if (err) {
                                    throw err;   
                                } else {
                                    res.json({success: true, message: 'Data Liked Successfully'});
                                }
                            });
                          
                        } else {
                          var b = blog.likes;
                          b++;           
                        db.collection('blogs').update({_id: mongojs.ObjectId(req.body.id)},
                          { $push: { 'likedBy': req.body.name},
                           $set: {'likes': b} },

                           function(err, data)  {
                             if (err) {
                                 throw err;   
                             } else {
                                 res.json({success: true, message: 'Data Liked Successfully'});
                             }
                         });
                        }
                    }
                }
            }
        });
    }
});

router.put('/dislikeBlog', (req, res) => {
    if(!req.body.id) {
        res.json({success: false, message: 'No id found'});
    }
    else if(!req.body.name) {
        res.json({success: false, message: 'No name found'});
    } else {
        db.collection('blogs').findOne({_id: mongojs.ObjectId(req.body.id)} , (err, blog) => {
            if(err) {
                res.json({success: false, message: 'Invalid blog id'}); 
            } else {
                if(!blog) {
                    res.json({success: false, message: 'Blog not found'}); 
                } else {
                    if(blog.dislikedBy.includes(req.body.name))  {                     
                        res.json({success: false, message: 'You have aleady disliked it'});   
                     } else {
                        if(blog.likedBy.includes(req.body.name)) {
                          
                            var a = blog.likes;
                            a--;
                            var b = blog.dislikes;
                            b++;
                            db.collection('blogs').update({_id: mongojs.ObjectId(req.body.id)},
                             { $push: { 'dislikedBy': req.body.name},
                              $set: {'likes': a, 'dislikes': b}, 
                              $pull: { 'likedBy': req.body.name} },

                              function(err, data)  {
                                if (err) {
                                    throw err;   
                                } else {
                                    res.json({success: true, message: 'Data UnLiked Successfully'});
                                }
                            });
                          
                        } else {
                          var b = blog.dislikes;
                          b++;           
                        db.collection('blogs').update({_id: mongojs.ObjectId(req.body.id)},
                          { $push: { 'dislikedBy': req.body.name},
                           $set: {'dislikes': b} },

                           function(err, data)  {
                             if (err) {
                                 throw err;   
                             } else {
                                res.json({success: true, message: 'Data UnLiked Successfully'});
                             }
                         });
                        }
                    }
                }
            }
        });
    }
});
 

router.post('/comment', (req, res) => {
    if (!req.body.comment) {
        res.json({success: false, message: 'No comment provided'});
    } else {
        if(!req.body.name) {
            res.json({success: false, message: 'No name provided'});
        } else {
            if(!req.body.id) {
                res.json({success: false, message: 'No id provided'});
            } else {   
                    const obj = {
                        comment: req.body.comment,
                        commentator: req.body.name
                    }
                    db.collection('blogs').update({_id: mongojs.ObjectId(req.body.id)},
                    { $push: { 'comments': obj}},
                     function(err, data)  {
                       if (err) {
                           throw err;   
                       } else {
                          res.json({success: true, message: data});
                       }
                   });
                }
            }
        }
    });
    
module.exports = router;