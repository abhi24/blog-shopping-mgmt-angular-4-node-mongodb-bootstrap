var mongoose = require('mongoose');
var Scheme = mongoose.Schema;

var blogSchema = new Scheme({
    title: {type: String, required: true},
    body: {type: String, required: true},
    createdBy: {type: String, required: true},
    createdAt: {type: String},
    likes: {type: Number, default: 0},
    likedBy: {type: Array},
    dislikes: {type: Number, default: 0},
    dislikedBy: {type: Array},
    comments: [{
        comment: {type:String},
        commentator: {type: String}
    }]
    
});

module.exports = mongoose.model('Blog', blogSchema);