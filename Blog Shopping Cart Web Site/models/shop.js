var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var shop = new Schema({
    imagePath: {type: String, required: true},
    name: {type: String, required: true},
    description: {type: String, required: true},
    amount: {type: Number, required: true},
    
});

module.exports = mongoose.model('shop', shop)