
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './component/home/home.component';
import {DashboardComponent} from './component/dashboard/dashboard.component';
import {RegisterComponent} from './component/register/register.component';
import { LoginComponent } from './component/login/login.component';
import { BlogComponent } from './component/blog/blog.component';
import { ProfileComponent } from './component/profile/profile.component';
import {AuthGuard} from './guards/auth.guard';
import { EditBlogComponent } from './component/edit-blog/edit-blog.component';
import { ShoppingCartComponent } from './component/shopping-cart/shopping-cart.component';
import { CheckoutComponent } from './component/checkout/checkout.component';



const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'dashboard', component: DashboardComponent ,canActivate: [AuthGuard]},
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
    { path: 'blog', component: BlogComponent },
    { path: 'shopping-cart', component: ShoppingCartComponent },
    { path: 'checkout', component: CheckoutComponent },
    
    { path: 'edit-blog/:id', component: EditBlogComponent },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
    {path: '*', component: HomeComponent}
  ];
  
@NgModule({
imports: [RouterModule.forRoot(appRoutes)],
exports: [RouterModule]
})

export class AppRoutingModule {

}