import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class BlogService {
  constructor(private http: Http) {}
   
  postBlog(blog){
      return this.http.post('http://localhost:3000/blogs/newBlog', blog).map(data => data.json())
    }

    getallBlogs() {
      return this.http.get('http://localhost:3000/blogs/allBlogs').map(data => data.json());
    }

    getSingleBlog(id) {
      return this.http.get('http://localhost:3000/blogs/singleBlog/'+id).map(data => data.json());
    }
    updateBlog(id, title, body) {
     // console.log(id, title, body);
      const blog = {
        _id: id,
        title: title,
        body: body
      }
     return this.http.put('http://localhost:3000/blogs/updateBlog/', blog).map(data => data.json());
    }

    deleteBlog(id) {
      const blog = {
        id : id
      }
   //   console.log('http://localhost:3000/blogs/deleteBlog/'+id  + 'and id is ' + blog);
      return this.http.put('http://localhost:3000/blogs/deleteBlog/'+id, blog).map(data => data.json());
    }

    sendLikes(blogId, name) {
        var obje = {
         id: blogId,
         name: name
        }
        // console.log('Pikachi');
        // console.log(obje);
        return this.http.put('http://localhost:3000/blogs/likeBlog', obje).map(data => data.json());
    }

    senddisLikes(blogId, name) {
      var obje = {
        id: blogId,
        name: name
       }
     
       return this.http.put('http://localhost:3000/blogs/dislikeBlog', obje).map(data => data.json());
    }

    postComment(id, name, comment) {
      const blogData = {
        id: id,
        name: name,
        comment: comment
      }
      return this.http.post('http://localhost:3000/blogs/comment', blogData).map(data => data.json());
    }
}
