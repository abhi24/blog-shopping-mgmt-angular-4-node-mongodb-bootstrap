import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class AuthService {
authToken;
userValue;
options;
  constructor(private http: Http) {

   }
   registerUser(user) {
      return this.http.post('http://localhost:3000/authentication/register',user)
      .map(response => response.json());
   }

    login(user) {
      var res = this.http.post('http://localhost:3000/authentication/login', user).map(res => res.json());
      return res;
    }

    sendLikes(likeNumber, nameOfLiker) {
      console.log(likeNumber, nameOfLiker);
    }
    
    postBlog(blog){
      return this.http.post('http://localhost:3000/blogs/newBlog', blog).map(data => data.json())
    }
    isAuthenticated() {
      return this.authToken != null;
    }

    storeuserData(token, user, usersss, passss, Value) {
        localStorage.setItem('token', token);
        localStorage.setItem('user', usersss);
        this.authToken = token;
        this.userValue = passss;
        // console.log('game');
        // console.log(this.authToken, + '     '+ this.userValue);
    }

    getProfile() {
      this.loadToken();     
 //     console.log('Value is ' + this.userValue);
      const va = {
        username: this.userValue
      }
        var res = this.http.post('http://localhost:3000/authentication/profile/', va)
        .map(data => data.json());
       return res;
    }
    logout() {
    //  console.log('Logout');
      localStorage.clear();
      this.authToken =null;
    }

    loadToken() {
      const token =  localStorage.getItem('token');
      const user = localStorage.getItem('user');
      this.authToken = token;
      this.userValue = user;
   //   console.log(user);
}
 
   
}
