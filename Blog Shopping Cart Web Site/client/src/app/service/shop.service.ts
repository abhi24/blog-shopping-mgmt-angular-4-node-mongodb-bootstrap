import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class ShoppingService {
    productsUserSelected= [];
     total = 0;
    constructor(private http: Http) {
    }
    getProducts() {
        return this.http.get('http://localhost:3000/shops/getAllProducts').map(data => data.json());
    }

    productsSelected(id) {
        this.productsUserSelected.push(id);
    }
    getTotalProducts() {
        return this.productsUserSelected;
    }

    sendProductstoServer(id) {
          return this.http.get('http://localhost:3000/shops/getProduct/'+id).map(data => data.json());
    }
    resetCart() {
        this.productsUserSelected = [];
    }
    deleteItemFromCart(id) {
        this.productsUserSelected.splice(id, 1);
    }
    onCheckoutDone(amount) {
        this.total =  amount;
    }
}