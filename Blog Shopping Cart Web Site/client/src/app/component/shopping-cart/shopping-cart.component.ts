import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ShoppingService } from '../../service/shop.service';
import { RouterModule, Router } from '@angular/router';
import {ProductType}  from './product.model';
@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  itemIncart= [];
  itemsTotalfromServer= [];
   finalProduct= [];
   totalPrice = 0;
   p;
  constructor(private shopSer: ShoppingService, private router: Router) { }

  ngOnInit() {
    this.itemIncart = this.shopSer.getTotalProducts();

    for(var i = 0; i <this.itemIncart.length ;i++) {
      this.shopSer.sendProductstoServer(this.itemIncart[i]).subscribe((data) => {
      
        this.p = {
          name: data.message.name,
          amount: data.message.amount, 
          quantity: 0,
          id: data.message._id,
        };

        this.finalProduct.push(this.p);     
        this.itemsTotalfromServer.push(data);
        this.totalPrice = this.totalPrice + (+data.message.amount);
      });      
    } 
  }

  
onDeletePressed(id) {
  console.log(id);
  for(var i=0; i<this.itemsTotalfromServer.length; i++)
  {
    if(this.itemsTotalfromServer[i].message._id === id)
    {
      this.totalPrice = this.totalPrice - this.itemsTotalfromServer[i].message.amount;
      this.itemsTotalfromServer.splice(i, 1);  
      this.shopSer.deleteItemFromCart(i);  
        break;
    }
  }
}

checkoutPressed() {
  this.shopSer.onCheckoutDone(this.totalPrice);
  this.router.navigate(['/checkout']);
}

onClearCartPressed() {
  this.itemsTotalfromServer = [];
  this.totalPrice =0;
  this.shopSer.resetCart();
}
}