export class ProductType {
    name: String;
    amount: String;
    quantity: Number;
    id: String;
}