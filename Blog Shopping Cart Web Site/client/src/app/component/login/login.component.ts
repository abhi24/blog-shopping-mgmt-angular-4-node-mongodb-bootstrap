import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { RouterModule, Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  addDataForm: FormGroup;
  message: String;
  messageClass;

  constructor(private authSer: AuthService, private router: Router) { }

  ngOnInit() {
    this.addDataForm = new FormGroup({
      'username': new FormControl(null),
      'password': new FormControl(null)
    });
  }
  onSubmit() {
      this.authSer.login(this.addDataForm.value).subscribe(data => {
        if(!data.success) {
          this.message = data.message;
        } else {
          this.message = data.message;

          this.authSer.storeuserData(data.token, data.user, data.passss, data.usersss, data.val)
          this.router.navigate(['/home']);
          window.localStorage.setItem('nameIs', data.user.username);
          window.localStorage.setItem('emailIs', data.val.email);
          
        }
      });
         
  }

}
