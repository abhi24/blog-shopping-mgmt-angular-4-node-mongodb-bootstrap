import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profileData;
  usernameVal: String;
  emailVal: String;
  constructor(private authSer: AuthService) { }

  ngOnInit() {

    if(window.localStorage.getItem('nameIs')){
      this.usernameVal = window.localStorage.getItem('nameIs');
      this.emailVal = window.localStorage.getItem('emailIs');
    }
    else {
      this.authSer.getProfile().subscribe(data => {
        
          this.profileData = data;
          console.log(this.profileData);
          console.log(this.profileData.pass.password);
          this.usernameVal = this.profileData.pass.password;
          this.emailVal = this.profileData.email.email;
      });
    }

  }

}
