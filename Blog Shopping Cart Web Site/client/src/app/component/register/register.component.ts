import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../service/auth.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  addDataForm: FormGroup;
  message: String;
  messageClass;
  constructor(private authSer: AuthService) { }

  ngOnInit() {
    this.addDataForm = new FormGroup({
      'email': new FormControl(null, Validators.required),
      'username': new FormControl(null,  Validators.required),
      'password': new FormControl(null,  Validators.required),
    });
    
  }

  disableForm() {
    this.addDataForm.controls['email'].disable();
    this.addDataForm.controls['username'].disable();
    this.addDataForm.controls['password'].disable();
  }
  onSubmit() {
    console.log(this.addDataForm);
    this.authSer.registerUser(this.addDataForm.value).subscribe(data => {
      if(!data.success){
        this.messageClass = 'alert alert-danger';
        this.message = data.message;
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        this.disableForm();
      }
    });
  }
}
