import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  authenticationDone= false;
  constructor(private authSer: AuthService) { }

  ngOnInit() {
  }
  onLogoutPressed() {
    this.authSer.logout();
  }
  onLoginPressed() {
    this.authenticationDone = true;
  }
}
