import { Component, OnInit, NgZone , ChangeDetectorRef} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { BlogService } from '../../service/blog.service';
import { AuthService } from '../../service/auth.service';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  newPost = true;
  commentButtonPressed= false;
  blogForm: FormGroup;
  commentFormValue: FormGroup;
  message;
  messageClass;
  processedForm = true;
  blogPost: any;
  NgZone : NgZone;
  editedByIdvalue;
  finalvalue: any;
  editItemStarted = false;
  enabledComments = [];
commentId;

   a: String;
   newComment = [];
 userloggedin;

  constructor(private blogSer: BlogService, private authSer: AuthService, private router: Router, private zone: NgZone,private _cdRef: ChangeDetectorRef) { 
    this.createBlogForm();
    this.commentForm();
    }
  
  loadingBlogs = false;

  ngOnInit() {
 
    this.getAllBlogs(); 
    this.a = window.localStorage.getItem('nameIs');
    }

   onEditClicked(editedById) {

     this.editedByIdvalue = editedById;
     this.editItemStarted = true;
     this.init(editedById);
   }

   onDeleteClicked(deleteById) {
    if(confirm("Are you sure to delete:  "+deleteById)) {
    //  console.log("Implement delete functionality here");
    }

    for(const l of this.blogPost.message) {
      if(l._id === deleteById) {
       this.blogSer.deleteBlog(deleteById).subscribe((data) => {
       });
      } else {
        console.log('Sorry try again');
      }
        }
    window.location.reload();
    this.router.navigate(['/blog']);
}

   init(editedById) {
     var idBody;
     var idTitle;
     this.blogSer.getallBlogs().subscribe((data) => {
      this.blogPost = data;
    })
    for(const l of this.blogPost.message) {
        if(l._id === editedById) {

          this.newPost = false;
          idTitle = l.title;
          idBody = l.body;
        } else {
          console.log('Sorry try again');
        }
    }
    this.blogForm = new FormGroup({
      'title': new FormControl(idTitle),
      'body': new FormControl(idBody)
    });
   }
  
   getAllBlogs() {
    this.blogSer.getallBlogs().subscribe((data) => {
      this.blogPost = data;
    })
  }

  onLikePressed(blogIdNumber) {
      this.blogSer.sendLikes(blogIdNumber, this.a).subscribe((data) => {
        if(!data.success){
          this.messageClass = 'alert alert-danger';
          this.processedForm = !this.processedForm;
          this.disableForm();
          this.message = data.message;
        } else {
          this.messageClass = 'alert alert-success';
          this.message = data.message;
          this.disableForm();
          this.processedForm = !this.processedForm;
        }
      });  
      this.disableForm();
  }

  ondisLikePressed(blogIdNumber) {
    this.blogSer.senddisLikes(blogIdNumber, this.a).subscribe((data) => {
      if(!data.success){
        this.messageClass = 'alert alert-danger';
        this.processedForm = !this.processedForm;
        this.disableForm();
        this.message = data.message;
      } else {
        this.messageClass = 'alert alert-success';
        this.message = data.message;
        this.disableForm();
        this.processedForm = !this.processedForm;
      }
    });
    this.disableForm();
  }



  disableForm() {
    this.blogForm.controls['title'].disable();
    this.blogForm.controls['body'].disable();
  }

  onSubmit() {
    const obj = {
      title: this.blogForm.get('title').value,
      body: this.blogForm.get('body').value,
      createdBy: this.a
    }
    if(this.editItemStarted) {  
        this.blogSer.getSingleBlog(this.editedByIdvalue).subscribe(data => {
          if(data.success) {
            this.blogSer.updateBlog(this.editedByIdvalue, this.blogForm.get('title').value, this.blogForm.get('body').value ).subscribe((data) =>{
              this.messageClass = 'alert alert-success';
              this.message = data.message;
              this.disableForm();
              this.processedForm = !this.processedForm;
              setTimeout(function(){
                window.location.reload();
              },2000);

              this.router.navigate(['/blog']);
            })          
          } else {
            console.log('Not foundddd');
          }
        });

    } else {
      this.blogSer.postBlog(obj).subscribe((data) => {
        if(!data.success){
          this.messageClass = 'alert alert-danger';
          this.message = data.message;
        } else {
          this.messageClass = 'alert alert-success';
          this.message = data.message;
          this.disableForm();
          this.processedForm = !this.processedForm;
        }
      });
      this.message= 'Blog Posted';
      this.disableForm();
    }  
  }

  newBlogForm() {
    this.newPost = false;
  }
  ongoback() {
    this.newPost = true;
  }
  createBlogForm() {
    this.blogForm = new FormGroup({
      'title': new FormControl(null),
      'body': new FormControl(null)
    });
  }
  readBlogs() {
    this.loadingBlogs = true;
    setTimeout(()=> {
      this.loadingBlogs = false;
     this.a = window.localStorage.getItem('nameIs');
    },2000)
  }

  postComment(id) {
    this.commentId = id;
  }

  cancelComment(id) {
    const index = this.newComment.indexOf(id);
    this.newComment.splice(index, 1);
    this.commentFormValue.reset();
  }
  commentForm() {
    this.commentFormValue = new FormGroup({
      'comment': new FormControl(null),
    });
    
  }

  draftComment(id) {
    this.newComment = [];
    this.newComment.push(id);
  }

  onCommentFormSubmit() {
   // console.log(this.commentFormValue.value);
    var ab= this.commentFormValue.get('comment').value;
    this.blogSer.postComment(this.commentId, this.a , ab).subscribe((data) => {
        console.log(data);
    });
  }

  expand(id) {
    this.enabledComments.push(id);
   // console.log('Size is expand' + this.enabledComments.length);    
  }

  collapse(id) {
    const index = this.enabledComments.indexOf(id);
    // console.log(index);
    // console.log('Size is' + this.enabledComments.length);   
    this.enabledComments.splice(index, 1); 
    // console.log('Size is collapse' + this.enabledComments.length);
  }
}
