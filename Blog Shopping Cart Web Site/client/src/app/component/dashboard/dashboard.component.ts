import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ShoppingService } from '../../service/shop.service';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  product = [];
  constructor(private shopSer: ShoppingService ) { }

  ngOnInit() {
    this.init();
  }

  init() {
       this.shopSer.getProducts().subscribe((data) => {   
          this.product = data;
       });
  }
  onAddPressed(id) {
    this.shopSer.productsSelected(id);
  }

}
