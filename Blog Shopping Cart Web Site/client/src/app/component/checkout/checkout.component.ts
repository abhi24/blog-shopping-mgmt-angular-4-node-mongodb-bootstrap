import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ShoppingService } from '../../service/shop.service';
import { RouterModule, Router } from '@angular/router';


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  total = 0;
  message;
  messageClass;
  paymentForm: FormGroup;
  constructor(private shopSer: ShoppingService, private router: Router) { }

  ngOnInit() {
   
    this.total = this.shopSer.total;
    console.log(this.total);
    this.init();
  }
  init() {
    this.paymentForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'address': new FormControl(null, Validators.required),
      'cardname': new FormControl(null, Validators.required),
      'cardnumber': new FormControl(null, [
        Validators.required,
       ]),
      'cardexpirymonth': new FormControl(null, Validators.required),
      'cardexpiryyear': new FormControl(null, Validators.required),
      'cardcvc': new FormControl(null, Validators.required),
    });
  }
  onSubmit() {
      console.log(this.paymentForm.value);
    this.message= 'Payement Successfully Made';
      setTimeout(() => {
        this.router.navigate(['/dashboard']);
      }, 3000);
      
  }

}
